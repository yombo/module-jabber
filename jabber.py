#This file was created by Yombo for use with Yombo Gateway automation
#software.  Details can be found at http://www.yombo.net
"""
Jabber
======

Implements an interface between Jabber and YomboBot module. Allows
interactions between the YomboBot module through a Jabber chat
session.

.. warning::

   Jabber should not be considered a secure interface, use with
   caution!

.. warning::

   This jabber client uses the Wokkel tools to make life easier/faster.
   You must have the wokkel module installed. Typically
   ```sudo apt-get install python-wokkel``` (or equivalent) will do.

License
=======

This module is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The **`Yombo.net <http://www.yombo.net/>`_** team and other contributors
hopes that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

The GNU General Public License can be found here: `GNU.Org <http://www.gnu.org/licenses>`_

.. moduleauthor:: Mitch Schwenk <mitch-gw@yombo.net>
:copyright: Copyright 2012-2015 by Yombo.
:license: GPL(v3)
:organization: `Yombo <http://www.yombo.net>`_
"""

import time
from collections import OrderedDict
from wokkel.client import XMPPClient
from wokkel.subprotocols import XMPPHandler
from wokkel.xmppim import MessageProtocol, AvailablePresence, PresenceProtocol, UnavailablePresence, RosterClientProtocol

from twisted.internet import task
from twisted.words.protocols.jabber import jid
from twisted.words.xish import domish

from yombo.core.exceptions import YomboModuleWarning
from yombo.core.log import get_logger
from yombo.core.module import YomboModule
from yombo.utils import generate_uuid
from yombo.utils.fuzzysearch import FuzzySearch

logger = get_logger('modules.jabber')

class YomboJabberProtocol(MessageProtocol):
    def setVariables(self, allowedUsers, yomboBot, jabberModule):
        self._FullName = "yombo.gateway.modules.jabber.jabberprotocol"
        self.yomboBot = yomboBot
        self.allowedUsers = allowedUsers
#        self.UserJabberID = variables['UserJabberID']['value']
#        self.UserJabberPIN = variables['UserJabberPIN']['value']
        self.BadUser = [] # Remember list of users we send not auth'd msg to.
#        self.UserSessions = SQLDict(self, "UserSessions")
        self.UserSessions = {}
        self.jabberSessionPINCount = 0
        self.jabberModule = jabberModule

#        self._userSettings = SQLDict(self, "usersettings") # 'self' is required for data isolation

    def connectionMade(self):
        logger.info("YomboBot connected with Jabber Server.")
        self.times = self.jabberModule._Libraries['times']
        self.send(AvailablePresence(statuses={None: 'Ready for requests'}))  # send initial presence

    def sayGoodbye(self):
        self.send(UnavailablePresence())  # send initial presence

    def connectionLost(self, reason):
        logger.info("YomboBot lost connection with Jabber Server.  Will attempt reconnect.")

    def _getReply(self, msg):
        reply = domish.Element((None, "message"))
        reply["to"] = msg["from"]
        reply["from"] = msg["to"]
        reply["type"] = 'chat'
        return reply
    
    def onMessage(self, msg):
        """
        This is called when a message comes in from a remote client.
        """
        msgbody = str(msg.body)
        logger.info("got msg subject: {msgsubject}", msgsubject=msg.subject)
#        logger.info("got msg type: %s", msg['type'])

        if msg["type"] != 'chat':
            return

        if msg.body == None:
            return
        
        if hasattr(msg, "body") == False:
            reply = self._getReply(msg)
            reply.addElement("body", content="Couldn't process the request.")
            reply = self._getReply(msg)
            self.send(reply)
            return

        logger.info("got msg body: {msgbody}", msgbody=msg.body)

        from_id = msg["from"]
        from_id_test = from_id.split("/")
        from_id_test = from_id_test[0]

        if from_id_test not in self.allowedUsers:  #ignore everyone unless it's the boss
            if from_id_test not in self.BadUser:
                self.BadUser.append(from_id_test)
                reply = self._getReply(msg)
                reply.addElement("body", content="You're not authorized. Add this ID to list of authorized IDs.")
                self.send(reply)
            return

#        # we are allowed. Lets get the user index, and find any matching pin.
#        userIndex = self.UserJabberID.index(from_id_test)
#        userPin = self.UserJabberPIN[userIndex]

        if from_id not in self.UserSessions:
            logger.debug("New jabber session allowed from id: {msg}", msg=msg["from"])
            self.UserSessions[from_id] = { 'sessionTime' : int(time.time()),
                                           'sessionID' : generate_uuid(),
                                           'pinCount' : 0,
                                           'auth' : 0,
                                           'new' : 1}

        if self.UserSessions[from_id]['auth'] == 0:
            if msgbody == self.allowedUsers[from_id_test]:
                self.UserSessions[from_id]['pinCount'] = 0
                self.UserSessions[from_id]['auth'] = 1
                reply = self._getReply(msg)
                reply.addElement("body", content="For jabber commands, type ':help'. For YomboBot/Voice Commands help, type '.help' (without quotes).")
                self.send(reply)
                self.yomboBot.registerConnection(source=self, sessionid=self.UserSessions[from_id]['sessionID'], authuser="UNKNOWN", remoteuser=from_id)
                return
            else:
                self.UserSessions[from_id]['pinCount'] = self.UserSessions[from_id]['pinCount'] + 1
                self.UserSessions[from_id]['auth'] = 0
                if self.UserSessions[from_id]['pinCount'] > 4:
                    logger.warn("overload timeout: {time} < {timeout}", time=self.UserSessions[from_id]['pinTimeout'], timeout=int(time.time()-10))
                    if self.UserSessions[from_id]['pinCount'] == 5:
                        reply = self._getReply(msg)
                        reply.addElement("body", content="Too many invalid attempts. Blocking for a bit.")
                        self.send(reply)
                        return

                    if self.UserSessions[from_id]['pinTimeout'] < int(time.time()) - 10:
                        self.UserSessions[from_id]['new'] = 1
                        self.UserSessions[from_id]['pinTimeout'] = int(time.time())
                        logger.warn("overload! {usersession}", usersession=self.UserSessions[from_id])
                    else:
                        return
                reply = self._getReply(msg)
                if self.UserSessions[from_id]['new'] == 1:
                    self.UserSessions[from_id]['new'] = 0
                    self.UserSessions[from_id]['pinTimeout'] = int(time.time())
                    reply.addElement("body", content="New session. Enter security code to continue. %s" % from_id)
                    self.send(reply)
                else:
                    reply.addElement("body", content="Invalid password.")
                    self.send(reply)
                return

        if int(time.time()) > self.UserSessions[from_id]['sessionTime'] + 900:
            if msgbody == self.allowedUsers[from_id_test]:
                reply = self._getReply(msg)
                self.UserSessions[from_id]['sessionTime'] = int(time.time())
                reply.addElement("body", content="Password accepted, session extended.")
                self.send(reply)
            else:
                reply = self._getReply(msg)
                reply.addElement("body", content="Session expired. Enter security code to continue.")
                self.send(reply)
            return

        self.UserSessions[from_id]['sessionTime'] = int(time.time())

        jabberCommands = FuzzySearch({
          ":help" : "This is the help summary. Additional \r\n   " \
            "details can be found by type the  associated \r\n   " \
            "command help.  Commands with * indicate lots \r\n   " \
            "of entries will be displayed.",
          ":settings" : "Configure settings.",
          ":exit" : "Exit current session.",
          }, .80)

        if msgbody[0] == ':':  ## process help/local commands/debug..
            cmdKey = ''
            try:
                cmdKey = jabberCommands.get_key(msgbody)
                if cmdKey == ":help":
                    body = ''
                    for item in jabberCommands:
                        body = "\r\n".join("'%s' - %s" % (k,v) for k, v in jabberCommands.items())
                    reply = self._getReply(msg)
                    reply.addElement("body", content=body)
                    self.send(reply)
                    return
#                if cmdKey == ":settings":
                    
#                    reply.addElement("body", content=body)
#                    self.send(reply)
#                    return
                if cmdKey == ":exit":
                    del self.UserSession[from_id]
                    self.yomboBot.registerConnection(sessionid=self.UserSessions[from_id]['sessionID'])
                    body = "Good bye"
                    reply = self._getReply(msg)
                    reply.addElement("body", content=body)
                    self.send(reply)
                    return
                
#                self._userSettings
            except:
                body = "Jabber - Exception processing command. Aborted. %s" % (msgbody,)
                reply = self._getReply(msg)
                reply.addElement("body", content=body)
                self.send(reply)
                return                
        # end of protocol handler.  Now pass the message to the Yombo Bot!
        self.jabberModule.yomboBotSend(self.UserSessions[from_id]['sessionID'], msgbody, msg["to"], msg["from"])

    def BotResponse(self, sessionid, lineout, msgto, msgfrom):
        if msgto not in self.UserSessions:
            logger.info("Can't send lineout to invalid session. '{lineout}'", lineout=lineout)
            return
        logger.debug("BR: to:{msgto}, from:{msgfrom}, msg:{lineout}", msgto=msgto, msgfrom=msgfrom, lineout=lineout)
        themsg = domish.Element((None, "message"))
        themsg["to"] = msgto
        themsg["from"] = msgfrom
        themsg["type"] = 'chat'
        themsg.addElement("body", content=lineout)
        self.send(themsg)


class PresenceAcceptingHandler(PresenceProtocol):
    """
    Presence accepting XMPP subprotocol handler.

    Only response to presence for allowed users.
    """
    def setVariables(self, allowedUsers, jabber):
        logger.debug("presense Allowed Users: {allowedUsers}", allowedUsers=allowedUsers)
        self.allowedUsers = allowedUsers
        self.jabberReference = jabber

    def subscribedReceived(self, presence):
        """
        Subscription approval confirmation was received.

        This is just a confirmation. Don't respond.
        """
        pass


    def unsubscribedReceived(self, presence):
        """
        Unsubscription confirmation was received.

        This is just a confirmation. Don't respond.
        """
        pass


    def subscribeReceived(self, presence):
        """
        Subscription request was received.

        Always grant permission to see our presence.
        """
        #logger.info("Jabber users: %s", self.UserJabberID)
        #logger.info("Subscribe Received: %s", presence.sender.userhost())
        if presence.sender.userhost() in self.allowedUsers:
            logger.debug("Allowing subscribed... {userhost}", userhost=presence.sender.userhost())
            self.subscribed(recipient=presence.sender,
                        sender=presence.recipient)
            self.available(recipient=presence.sender,
                       status="I'm here",
                       sender=presence.recipient)
            self.jabberReference.subscribers[presence.sender.userhost()] = int(time.time())

    def unsubscribeReceived(self, presence):
        """
        Unsubscription request was received.

        Always confirm unsubscription requests.
        """
        self.unsubscribed(recipient=presence.sender,
                          sender=presence.recipient)
        if presence.sender.userhost() in self.jabberReference.subscribers:
            del self.jabberReference.subscribers[presence.sender.userhost()]

    def probeReceived(self, presence):
        """
        A presence probe was received.

        Always send available presence to whoever is asking.
        """
        logger.debug("Got a probe from: {userhost}", userhost=presence.sender.userhost())
        if presence.sender.userhost() in self.jabberReference.subscribers:
            self.available(recipient=presence.sender,
                       status="I am here",
                       sender=presence.recipient)

#Maybe used later?
class RosterHandler(RosterClientProtocol):
    def gotRoster(self, roster):
        print('Got roster:')
        for entity, item in roster.items():
            print('  %r (%r)' % (entity, item.name or ''))

    def gotRosterError(self, output):
        print('Got roster err:', output)

    def connectionInitialized(self):
        print('Getting roster...')
        RosterClientProtocol.connectionInitialized(self)
        d = self.getRoster()
        d.addCallback(self.gotRoster)
        d.addErrback(self.gotRosterError)


class Jabber(YomboModule):
    """
    Jabber Yombo Module

    This class is a simple wrapper around the JabberClient service, which
    handles the work.
    """

    def _init_(self):
        """
        Setup the module, prepare the jabber client service.
        """
        self._ModDescription = "Jabber to YomboBot gateway."
        self._ModAuthor = "Mitch Schwenk @ Yombo"
        self._ModUrl = "https://yombo.net"
        self.msgIDs = OrderedDict()
        self.subscribers = {}
#        self.subscribers = SQLDict(self, "subscribers")
#        self._States['hello4'] = "bob"

        self.jabberClient = None
        self._startable = False
        try:
            self.YomboBot = self._Modules['yombobot']
            self._startable = True
        except:
            logger.warn("Jabber module can't start due to no YomboBot module loaded.")

    def _load_(self):
        """
        Get our initial configurations for the client service.
        """
#        logger.info("States!!!! = {state}", state=self._States.get())
#        logger.info("Atoms!!!! = {atoms}", atoms=self._Atoms.get())
        if self._startable == False:
            return

#        logger.info("jabber module vars: {modVariables}", modVariables=self._ModuleVariables)
        if self._ModuleVariables == None:
            logger.error("Jabber module cannot load, required module variables missing.")
            return
        if 'GWJabberID' not in self._ModuleVariables or len(self._ModuleVariables['GWJabberID'][0]['value']) != 1:
            raise YomboModuleWarning('"GWJabberID" variable missing value.', 101, self)
        if str(self._ModuleVariables['GWJabberID'][0]['value']) == '':
            logger.error("Jabber module variable missing: Jabber ID")
            return

        if 'GWJabberPassword' not in self._ModuleVariables or len(self._ModuleVariables['GWJabberPassword'][0]['value']) != 1:
            raise YomboModuleWarning('"GWJabberPassword" variable missing value.', 100, self)
        if str(self._ModuleVariables['GWJabberPassword'][0]['value']) == '':
            logger.error("Jabber module variable missing: Jabber Account Password")

        if 'UserJabberID' not in self._ModuleVariables or len(self._ModuleVariables['UserJabberID'][0]['value']) == 0:
            raise YomboModuleWarning('"UserJabberID" variable missing value.', 100, self)
        if str(self._ModuleVariables['UserJabberID'][0]['value']) == '':
            logger.error("Jabber module variable missing: Allowed User ID")
            return

        if 'UserJabberPIN' not in self._ModuleVariables or len(self._ModuleVariables['UserJabberID'][0]['value']) != len(self._ModuleVariables['UserJabberPIN'][0]['value']):
            raise YomboModuleWarning('"UserJabberPIN" variable missing value.', 100, self)
        if str(self._ModuleVariables['UserJabberPIN'][0]['value']) == '':
            logger.error("Jabber module variable missing: User access PIN")
            return

        # create a mapping of remote users and their PINs
        self.allowedUsers =  {}
        for item, user in enumerate(self._ModuleVariables['UserJabberID']['value']):
            self.allowedUsers[user] = self._ModuleVariables['UserJabberPIN']['value'][item]
        self.xmppclient = XMPPClient(jid.internJID(self._ModuleVariables['GWJabberID'][0]['value']), self._ModuleVariables['GWJabberPassword'][0]['value'])
        self.xmppclient.logTraffic = False
        self.jabberProtocol = YomboJabberProtocol()
        self.jabberProtocol.setVariables(self.allowedUsers, self.YomboBot, self)
        self.jabberProtocol.setHandlerParent(self.xmppclient)
#        self.rosterHandler = RosterHandler()
#        self.rosterHandler.setHandlerParent(self.xmppclient)
        self.presenceHandler = PresenceAcceptingHandler()
        self.presenceHandler.setVariables(self.allowedUsers, self)
        self.presenceHandler.setHandlerParent(self.xmppclient)
        KeepAlive().setHandlerParent(self.xmppclient)
        self.xmppclient.startService()

    def _start_(self):
        self.BotReturn = getattr(self, "yomboBotReturn")

    def _unload_(self):
        try:
            self.xmppclient.stopService()
        except:
            pass

    # def _stop_(self):
    #     self.jabberProtocol.sayGoodbye()

    def Jabber_message_subscriptions(self, **kwargs):
        """
        hook_message_subscriptions called by the messages library to get a list of message types to be delivered here.

        YomboBot wants status messages to deliever to connected clients. Allows clients to get updates on device status.

        :param kwargs:
        :return:
        """
        return ['status', 'event']


    def message(self, message):
        if self._startable == False:
            return
        logger.debug("11111:: {message}", message=message)
        
#        logger.debug("Got an yombo message for jabber clients: %s", message)
#        if message.msgType == 'event' or message.msgType == 'status':
#            self.xmppclient.processMessage(message)
    
    def yomboBotSend(self, sessionID, lineIn, msgto, msgfrom):
        """
        Incomming message from a jabber client.  The user has been validated by the protocol
        if it's here already.
        
        Here, we simply pass any incoming right to YomboBot for processing.
        """
        msguuid = generate_uuid()
        self.msgIDs[msguuid] = {'msgfrom':msgfrom, 'msgto':msgto, 'sessionid':sessionID }
        if len(self.msgIDs) > 60:
            tossmeaway = self.msgIDs.popitem()
            logger.debug("SOMETHING WAS TOSSSSSEEEEDDDD!!! {tossmeaway}", tossmeaway=tossmeaway)
#        logger.debug("msgIDs: {msgIDs}", msgIDs=self.msgIDs)
        self.YomboBot.incoming(returncall=self.yomboBotReturn, linein=lineIn, msgid=msguuid, sessionid=sessionID)

    def yomboBotReturn(self, **kwargs):
        """
        yomboBot will return messages here as out "callback" method.
        """
        sessionid = kwargs['sessionid']
        msgid = kwargs['msgid']
        
        if "body" in kwargs:  #No processing needed, just send this to client.
            self.jabberProtocol.BotResponse(sessionid, kwargs['body'], self.msgIDs[msgid]['msgfrom'], self.msgIDs[msgid]['msgto']) #backwards!
        else: # something happened. Now lets process it
            response = kwargs['response']
            logger.debug("botreturnmsg: {response}", response=response)
            if response['msgStatus'] == 'done': # success!
                status = ''
                if 'newStatus' in response:
                    status = response['newStatus']
                else:
                    status = response['command']
                lineout = "%s is now %s" % (response['device'], status)
                self.jabberProtocol.BotResponse(sessionid, lineout, self.msgIDs[msgid]['msgfrom'], self.msgIDs[msgid]['msgto']) #backwards!
            else:
                lineout = "Status: %s" % (response['msgStatus'],)
                if 'msgStatusExtra' in response and response['msgStatusExtra'] is not None:
                    lineout = lineout + "\r\n Additional Information: %s" % (response['msgStatusExtra'],)
                lineout = lineout + "\r\n Device: %s\r\n Command: %s" % (response['device'], response['command'])
                self.jabberProtocol.BotResponse(sessionid, lineout, self.msgIDs[msgid]['msgfrom'], self.msgIDs[msgid]['msgto']) #backwards!
        
class KeepAlive(XMPPHandler):
    """
    Keep the connection to Jabber server alive. Send a ping every 250 seconds.
    """
    interval = 250
    lc = None

    def connectionInitialized(self):
        self.lc = task.LoopingCall(self.ping)
        self.lc.start(self.interval)

    def connectionLost(self, *args):
        if self.lc:
            self.lc.stop()

    def ping(self):
        logger.debug("Jabber keep alive sent...")
        self.send(" ")
